require("dotenv").config();

// override environment to production
process.env.BABEL_ENV = "production";
process.env.NODE_ENV = "production";

// crash on unhandled promise rejection
process.on("unhandledRejection", err => {
	throw err;
});

const fs = require("fs-extra");
const chalk = require("chalk");
const paths = require("../config/paths");
const webpack = require("webpack");
const config = require("../config/webpack.config")("production");
const fancy = require("./fancy");

const {
	measureFileSizesBeforeBuild,
	printFileSizesAfterBuild
} = require("./FileSizeReporter");

// These sizes are pretty large. We'll warn for bundles exceeding them.
const WARN_AFTER_BUNDLE_GZIP_SIZE = 512 * 1024;
const WARN_AFTER_CHUNK_GZIP_SIZE = 1024 * 1024;

async function run() {
	// get the total filesize of the previous build.
	// this only measures js and css file sizes
	const previousFileSize = await measureFileSizesBeforeBuild(paths.build);

	// empty build directory
	// or create a new one if it doesn't exist
	fs.emptyDirSync(paths.build);

	// copy files from the public folder to build folder
	fs.copySync(paths.public, paths.build, {
		dereference: true,
		// skip the template file file
		filter: file => file !== paths.template
	});

	fancy.space();
	fancy.log("Creating optimized production build...");
	fancy.space();

	// create webpack compiler
	const compiler = webpack(config);

	// run webpack
	compiler.run((err, stats) => {
		let messages;

		// if webpack gave an error
		if (err) {
			// if the error doesn't have a message, just print it
			if (!err.message) return buildError(err);

			messages = {
				errors: [err.message],
				warnings: []
			};
		} else
			messages = stats.toJson({
				all: false,
				warnings: true,
				errors: true
			});

		// throw error if webpack has any errors
		if (messages.errors.length)
			return buildError(new Error(messages.errors.join("\n\n")));

		// warnings found
		if (messages.warnings.length) {
			// if there are warnings and we are in a CI environment
			if (
				process.env.CI &&
				(typeof process.env.CI !== "string" ||
					process.env.CI.toLowerCase() !== "false")
			) {
				fancy.warn(
					"Treating warnings as errors because process.env.CI = true."
				);
				fancy.space();
				return buildError(new Error(messages.warnings.join("\n\n")));
			}

			fancy.warn("Compiled with warnings.");
			fancy.space();
			console.log(messages.warnings.join("\n\n"));
		} else {
			fancy.complete("Compiled successfully.");
		}

		fancy.space();
		fancy.info("File sizes after gzip:");
		fancy.space();
		printFileSizesAfterBuild(
			stats,
			previousFileSize,
			paths.build,
			WARN_AFTER_BUNDLE_GZIP_SIZE,
			WARN_AFTER_CHUNK_GZIP_SIZE
		);
		fancy.space();

		fancy.space();
		fancy.complete("Production build complete!");
		fancy.space();

		console.log(
			`The ${chalk.cyan(paths.build)} folder is ready to be deployed.`
		);

		fancy.space();
	});
}

function buildError(err) {
	fancy.error("Failed to compile.");
	fancy.space();

	const message = err != null && err.message;
	const stack = err != null && err.stack;

	// Add more helpful message for Terser error
	if (
		stack &&
		typeof message === "string" &&
		message.indexOf("from Terser") !== -1
	) {
		try {
			const matched = /(.+)\[(.+):(.+),(.+)\]\[.+\]/.exec(stack);
			if (!matched)
				throw new Error("Using errors for control flow is bad.");
			const problemPath = matched[2];
			const line = matched[3];
			const column = matched[4];
			console.log(
				"Failed to minify the code from this file: \n\n",
				chalk.yellow(
					`\t${problemPath}:${line}${
						column !== "0" ? ":" + column : ""
					}`
				),
				"\n"
			);
		} catch (ignored) {
			console.log("Failed to minify the bundle.", err);
		}
		console.log("Read more here: https://bit.ly/CRA-build-minify");
	} else console.log((message || err) + "\n");

	fancy.space();

	process.exit(1);
}

run();

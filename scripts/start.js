require("dotenv").config();

const paths = require("../config/paths");
const address = require("address");
const url = require("url");
const webpack = require("webpack");
// const child_process = require("child_process");
const webpackDevServer = require("webpack-dev-server");
const config = require("../config/webpack.config")("development");
const chalk = require("chalk");
const fancy = require("../scripts/fancy");

const appName = require(paths.packagejson).name || "";

const PORT = parseInt(process.env.PORT, 10) || 5000;
const HOST = process.env.HOST || "0.0.0.0";

const isInteractive = process.stdout.isTTY;
// const isInteractive = false;

const urls = prepareUrls(
	process.env.HTTPS === "true" ? "https" : "http",
	HOST,
	PORT
);

let compiler;

try {
	compiler = webpack(config);
} catch (err) {
	fancy.error("Failed to compile");
	fancy.space();
	console.log(err.message || err);
	fancy.space();
	process.exit(1);
}

// this fires when a file is changed
compiler.hooks.invalid.tap("invalid", () => {
	if (isInteractive) clearConsole();
	fancy.space();
	fancy.info("Compiling...");
});

var isFirstCompile = true;

compiler.hooks.done.tap("done", async stats => {
	if (isInteractive && !isFirstCompile) clearConsole();

	const { errors, warnings } = stats.toJson({
		all: false,
		warnings: true,
		errors: true
	});

	if (!errors.length && !warnings.length) {
		fancy.check("Compiled successfully!");
		if (isInteractive || isFirstCompile) {
			fancy.space();
			fancy.info(`${chalk.bold(appName)} is running at`);
			if (urls.lanUrlForTerminal) {
				console.log(
					`\t${chalk.bold("Local")} ${urls.localUrlForTerminal}`
				);
				console.log(
					`\t${chalk.bold("LAN")}   ${urls.lanUrlForTerminal}`
				);
			} else console.log(`\t${urls.localUrlForTerminal}`);
			fancy.space();
		}
		isFirstCompile = false;
		return;
	}
	isFirstCompile = false;

	if (errors.length) {
		// Only keep the first error. Others are often indicative
		// of the same problem, but confuse the reader with noise.
		if (errors.length > 1) errors.length = 1;
		fancy.error("Failed to compile");
		fancy.space(1);
		console.log(errors.join("\n"));
		return;
	}

	if (warnings.length) {
		fancy.warn(`Compiled with ${warnings.length} warnings`);
		fancy.space(1);
		console.log(warnings.join("\n"));
	}
});

const server = new webpackDevServer(compiler, {
	...config.devServer,
	contentBase: paths.public,
	clientLogLevel: "none",
	quiet: true,
	publicPath: "/",
	historyApiFallback: {
		disableDotRule: true
	},
	proxy: {
		"/.netlify": {
			target: "http://localhost:9000",
			pathRewrite: { "^/.netlify/functions": "" }
		}
	}
});

server.listen(PORT, HOST, err => {
	if (err) return console.log(err);
	fancy.space();
	fancy.info("Starting Webpack Dev Server...");
});

function prepareUrls(protocol, host, port) {
	const formatUrl = hostname =>
		url.format({
			protocol,
			hostname,
			port,
			pathname: "/"
		});
	const prettyPrintUrl = hostname =>
		url.format({
			protocol,
			hostname,
			port: chalk.bold(port),
			pathname: "/"
		});

	const isUnspecifiedHost = host === "0.0.0.0" || host === "::";
	let prettyHost, lanUrlForConfig, lanUrlForTerminal;
	if (isUnspecifiedHost) {
		prettyHost = "localhost";
		try {
			// This can only return an IPv4 address
			lanUrlForConfig = address.ip();
			if (lanUrlForConfig) {
				// Check if the address is a private ip
				// https://en.wikipedia.org/wiki/Private_network#Private_IPv4_address_spaces
				if (
					/^10[.]|^172[.](1[6-9]|2[0-9]|3[0-1])[.]|^192[.]168[.]/.test(
						lanUrlForConfig
					)
				) {
					// Address is private, format it for later use
					lanUrlForTerminal = prettyPrintUrl(lanUrlForConfig);
				} else {
					// Address is not private, so we will discard it
					lanUrlForConfig = undefined;
				}
			}
		} catch (_e) {
			// ignored
		}
	} else {
		prettyHost = host;
	}
	const localUrlForTerminal = prettyPrintUrl(prettyHost);
	const localUrlForBrowser = formatUrl(prettyHost);
	return {
		lanUrlForConfig,
		lanUrlForTerminal,
		localUrlForTerminal,
		localUrlForBrowser
	};
}

function clearConsole() {
	process.stdout.write(
		process.platform === "win32" ? "\x1B[2J\x1B[0f" : "\x1B[2J\x1B[3J\x1B[H"
	);
}

[![Netlify Status](https://api.netlify.com/api/v1/badges/e208888a-cad2-4e2c-acd9-ea197fee41c7/deploy-status)](https://app.netlify.com/sites/jacobcoughenour/deploys)

### Add a project

-   Create a new .md file in `src/projects/` and name it after the project id.

-   Add the required front-matter to the top of your new .md file.
	```markdown
	---
	gitlab_id: 1234545
	homepage: "http://piball.herokuapp.com/"
	---
	```
require("dotenv").config();

const path = require("path");
const paths = require("./paths");
// const webpack = require("webpack");

// webpack plugins
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const safePostCssParser = require("postcss-safe-parser");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = function(mode) {
	const isDev = mode === "development";

	process.env.BABEL_ENV = mode;
	process.env.NODE_ENV = mode;

	const publicPath = isDev ? "/" : paths.served;

	const env = {
		PUBLIC_PATH: publicPath,
		...process.env
	};

	return {
		mode,
		devtool: isDev && "source-map",
		entry: {
			bundle: ["whatwg-fetch", paths.index]
		},
		resolve: {
			alias: {
				svelte: path.resolve("node_modules", "svelte")
			},
			extensions: [".mjs", ".js", ".svelte"],
			mainFields: ["svelte", "browser", "module", "main"]
		},
		output: {
			path: isDev ? undefined : paths.build,
			pathinfo: isDev,
			filename: isDev
				? "static/js/bundle.js"
				: "static/js/[name].[contenthash:8].js",
			chunkFilename: isDev
				? "static/js/[name].chunk.js"
				: "static/js/[name].[contenthash:8].chunk.js",
			publicPath,
			devtoolModuleFilenameTemplate: isDev
				? info =>
						path
							.resolve(info.absoluteResourcePath)
							.replace(/\\/g, "/")
				: info =>
						path
							.relative(paths.src, info.absoluteResourcePath)
							.replace(/\\/g, "/"),
			globalObject: "this"
		},
		module: {
			rules: [
				{
					test: /\.(js|mjs|svelte)$/,
					include: paths.src,
					enforce: "pre",
					use: {
						loader: require.resolve("eslint-loader"),
						options: {
							formatter: "codeframe",
							eslintPath: require.resolve("eslint"),
							emitWarning: isDev
						}
					}
				},
				{
					test: /\.(js|mjs|svelte)$/,
					exclude: /node_modules\/(?!svelte)/,
					use: {
						loader: "babel-loader",
						options: {
							presets: ["@babel/preset-env"]
						}
					}
				},
				{
					test: /\.(svg)$/,
					loader: "svg-sprite-loader",
					options: {}
				},
				{
					test: /\.(html|svelte)$/,
					use: {
						loader: "svelte-loader",
						options: {
							emitCss: true,
							hotReload: true,
							preprocess: [require("svelte-preprocess")({})]
						}
					}
				},
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						"style-loader",
						MiniCssExtractPlugin.loader,
						{
							loader: "css-loader",
							options: {
								importLoaders: 1
							}
						},
						{
							loader: "sass-loader",
							options: {
								sassOptions: {
									includePaths: [
										"./src/theme",
										"./node_modules"
									]
								}
							}
						}
					]
				},
				{
					loader: "raw-loader",
					test: /\.(frag|vert)$/
				},
				{
					loader: "file-loader",
					exclude: [
						/\.(sa|sc|c)ss$/,
						/\.(js|mjs|jsx|ts|tsx)$/,
						/\.(html|svelte|ejs|md|svg)$/,
						/\.(frag|vert)$/,
						/\.json$/
					],
					options: {
						name: "static/media/[name].[hash:8].[ext]"
					}
				},
				{
					test: /\.(md)$/,
					loader: require.resolve("./ProjectLoader")
				}
			]
		},
		optimization: {
			minimize: !isDev,
			minimizer: [
				new TerserPlugin({
					terserOptions: {
						parse: {
							// We want terser to parse ecma 8 code. However, we don't want it
							// to apply any minification steps that turns valid ecma 5 code
							// into invalid ecma 5 code. This is why the 'compress' and 'output'
							// sections only apply transformations that are ecma 5 safe
							// https://github.com/facebook/create-react-app/pull/4234
							ecma: 8
						},
						compress: {
							ecma: 5,
							warnings: false,
							// Disabled because of an issue with Uglify breaking seemingly valid code:
							// https://github.com/facebook/create-react-app/issues/2376
							// Pending further investigation:
							// https://github.com/mishoo/UglifyJS2/issues/2011
							comparisons: false,
							// Disabled because of an issue with Terser breaking valid code:
							// https://github.com/facebook/create-react-app/issues/5250
							// Pending further investigation:
							// https://github.com/terser-js/terser/issues/120
							inline: 2
						},
						mangle: {
							safari10: true
						},
						// Added for profiling in devtools
						keep_classnames: false,
						keep_fnames: false,
						output: {
							ecma: 5,
							comments: false,
							// Turned on because emoji and regex is not minified properly using default
							// https://github.com/facebook/create-react-app/issues/2488
							ascii_only: true
						}
					},
					sourceMap: false
				}),
				new OptimizeCssAssetsPlugin({
					cssProcessorOptions: {
						parser: safePostCssParser,
						map: false
					},
					cssProcessorPluginOptions: {
						preset: [
							"default",
							{ minifyFontValues: { removeQuotes: false } }
						]
					}
				})
			],
			splitChunks: {
				chunks: "all",
				name: false
			},
			runtimeChunk: {
				name: entrypoint => `runtime-${entrypoint.name}`
			}
		},
		plugins: [
			new MiniCssExtractPlugin({
				filename: "static/css/[name].[contenthash:8].css",
				chunkFilename: "static/css/[name].[contenthash:8].chunk.css"
			}),
			new HtmlWebpackPlugin(
				Object.assign(
					{},
					{
						inject: true,
						template: paths.template,
						templateParameters: (
							compilation,
							assets,
							assetTags,
							options
						) => {
							return {
								compilation,
								webpackConfig: compilation.options,
								htmlWebpackPlugin: {
									tags: assetTags,
									files: assets,
									options
								},
								inject: {
									head: isDev
										? ""
										: `<script
	async
	src="https://www.googletagmanager.com/gtag/js?id=${env.GOOGLE_ANALYTICS_ID}"
></script>
<script>
	window.prerenderReady = false;
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		dataLayer.push(arguments);
	}
	function gtagSetPage(page) {
		gtag("config", "${env.GOOGLE_ANALYTICS_ID}", { "page_path": page });
	}
	gtag("js", new Date());
	gtag("config", "${env.GOOGLE_ANALYTICS_ID}");
</script>`
								},
								...env
							};
						}
					},
					isDev
						? undefined
						: {
								minify: {
									removeComments: true,
									collapseWhitespace: true,
									removeRedundantAttributes: true,
									useShortDoctype: true,
									removeEmptyAttributes: true,
									removeStyleLinkTypeAttributes: true,
									keepClosingSlash: true,
									minifyJS: true,
									minifyCSS: true,
									minifyURLs: true
								}
						  }
				)
			)
		].filter(Boolean),
		performance: false
	};
};

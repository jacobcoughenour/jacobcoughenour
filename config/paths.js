const path = require("path");
const fs = require("fs");
const url = require("url");
const appDirectory = fs.realpathSync(process.cwd());
const resolve = relativePath => path.resolve(appDirectory, relativePath);

const envPublicUrl = process.env.PUBLIC_URL;
const packageJson = require(resolve("package.json"));

// We use `PUBLIC_URL` environment variable or "homepage" field to infer
// "public path" at which the app is served.
// Webpack needs to know it to put the right <script> hrefs into HTML even in
// single-page apps that may serve index.html for nested URLs like /todos/42.
// We can't use a relative path in HTML because we don't want to load something
// like /todos/42/static/js/bundle.7289d.js. We have to know the root.
function getServedPath(packageJson) {
	// use PUBLIC_URL from env
	// or "homepage" from package.json
	// fallback on "/"
	const servedUrl =
		envPublicUrl || packageJson.homepage
			? new url.URL(packageJson.homepage).pathname
			: "/";

	// ensure that the path ends with a "/"
	return servedUrl.endsWith("/") ? servedUrl : `${servedUrl}/`;
}

const paths = {
	root: resolve("."),
	src: resolve("src"),
	index: resolve("src/index.js"),
	packagejson: resolve("package.json"),
	eslintconfig: resolve(".eslintrc.js"),
	build: resolve("build"),
	public: resolve("public"),
	template: resolve("public/index.ejs"),
	served: getServedPath(packageJson)
};

module.exports = paths;

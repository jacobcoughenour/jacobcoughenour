require("dotenv").config("");

if (!process.env.GITLAB_TOKEN)
	throw new Error("missing GITLAB_TOKEN env variable");
if (!process.env.NETLIFY_HOOK)
	throw new Error("missing NETLIFY_HOOK env variable");

const fancy = require("../scripts/fancy");
const fetch = require("node-fetch");
const loaderUtils = require("loader-utils");
const frontmatter = require("front-matter");
const markdownIt = require("markdown-it");
const markdownItAttrs = require("markdown-it-attrs");
const markdownItClass = require("@toycode/markdown-it-class");
const markdownItPrism = require("markdown-it-prism");

// json stringify and fixes line and paragraph separators
const stringify = src =>
	JSON.stringify(src)
		.replace(/\u2028/g, "\\u2028")
		.replace(/\u2029/g, "\\u2029");

// regex for testing if a path is a valid url
const isURLRegex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;

// setup our gitlab api headers
const gitlabHeaders = new fetch.Headers({
	"Content-Type": "application/json",
	// Authorization: process.env.GITLAB_TOKEN,
	"PRIVATE-TOKEN": process.env.GITLAB_TOKEN
});

// adds netlify hook if it doesn't already exist for this project
async function registerWebhook(projectRoute, projectName) {
	try {
		const json = await fetch(projectRoute + "/hooks", {
			method: "GET",
			headers: gitlabHeaders
		}).then(res => res.json());

		if (!Array.isArray(json)) return;

		const existing = json.find(
			hook => hook.url === process.env.NETLIFY_HOOK
		);

		// hook already exists
		if (existing) {
			// push events is not enabled
			if (!existing.push_events) {
				const putjson = await fetch(
					`${projectRoute}/hooks/${existing.id}?url=${process.env.NETLIFY_HOOK}&push_events=true`,
					{
						method: "PUT",
						headers: gitlabHeaders
					}
				).then(res => res.json());

				fancy.complete(
					"[project-loader] Updated Netlify hook for " + projectName
				);
			}
			return;
		}

		// add netlify webhook
		const postjson = await fetch(
			`${projectRoute}/hooks?url=${process.env.NETLIFY_HOOK}&push_events=true`,
			{
				method: "POST",
				headers: gitlabHeaders
			}
		).then(res => res.json());

		fancy.complete("[project-loader] Added Netlify hook to " + projectName);
	} catch (error) {
		console.error(error);
	}
}

async function fetchGitlabProject(gitlab_id) {
	// base api route for the project on gitlab
	const projectRoute = `https://gitlab.com/api/v4/projects/${gitlab_id}`;

	// fetch project info
	const {
		name,
		visibility,
		web_url,
		avatar_url,
		description,
		created_at,
		last_activity_at
	} = await fetch(projectRoute, {
		method: "GET",
		headers: gitlabHeaders
	}).then(res => res.json());

	// fetch project language stats
	let languages = await fetch(projectRoute + "/languages").then(res =>
		res.json()
	);

	// make the language keys lower case
	languages = Object.keys(languages).reduce(
		(a, key) => ((a[key.toLowerCase()] = languages[key]), a),
		{}
	);

	// log that we got the info from gitlab
	fancy.complete("[project-loader] Fetched info for " + name);

	// warn if project is not public
	if (visibility !== "public")
		fancy.warn(
			`[project-loader] Gitlab project ${name} is set to ${visibility}`
		);

	// ignore this project
	if (gitlab_id !== 12683327)
		// register the netlify webhook for this project
		registerWebhook(projectRoute, name);

	return {
		name,
		repo: web_url,
		icon: avatar_url,
		description: description || "",
		created_at,
		last_activity_at,
		languages
	};
}

/**
 * converts the markdown file into an esmodule as a string
 *
 * input markdown:
 * ```
 * ---
 * title: Hello World
 * ---
 * Rendered Markdown Text
 * ```
 *
 * output module string:
 * ```
 * "module.exports={
 *     attributes:{title:"Hello World"},
 *     html:"<p>Rendered Markdown Text</p>"
 * };"
 * ```
 */
async function processMarkdown(source) {
	// get the front-matter at the top of the file
	let { attributes, body } = frontmatter(source);

	// front-matter has a gitlab_id
	if (attributes.gitlab_id) {
		attributes = Object.assign(
			await fetchGitlabProject(attributes.gitlab_id),
			attributes
		);
	}

	// add frontmatter attributes to the export object
	const exported = `module.exports={attributes:${stringify(
		attributes
	)},html:`;

	// create our markdown compiler
	const markdownCompiler = markdownIt({ html: true });

	/**
	 * attributes plugin
	 *
	 * input:
	 * ```
	 * # header {.style-me}
	 * paragraph {data-toggle=modal}
	 * ```
	 *
	 * output:
	 * ```
	 * <h1 class="style-me">header</h1>
	 * <p data-toggle="modal">paragraph</p>
	 * ```
	 */
	markdownCompiler.use(markdownItAttrs);

	// true if the image loader plugin found any images in the body
	let hasImagesToRequire = false;

	/**
	 * image loader plugin
	 *
	 * input:
	 * ```![image](image.png)```
	 *
	 * output:
	 * ```<img src="${{{image.png}}}" alt="image" />```
	 */
	markdownCompiler.use(md => {
		const render =
			md.renderer.rules.image ||
			((tokens, idx, options, _env, self) =>
				self.renderToken(tokens, idx, options));

		// register our custom image rule
		md.renderer.rules.image = (tokens, idx, options, env, self) => {
			const token = tokens[idx],
				srcIndex = token.attrIndex("src"),
				src = token.attrs[srcIndex][1];

			// only change the src if it is a local file
			if (!isURLRegex.test(src)) {
				// normalize the file path for require()
				const request = loaderUtils.urlToRequest(src);
				// wrap the src attribute so we can inject the require() for it later
				token.attrs[srcIndex][1] = "${{{" + request + "}}}";
				// set flag for injecting after render
				hasImagesToRequire = true;
			}

			// pass the img to the next renderer
			return render(tokens, idx, options, env, self);
		};
	});

	markdownCompiler.use(markdownItClass, {
		h1: "mdc-typography--headline4",
		h2: "mdc-typography--headline5",
		h3: "mdc-typography--headline6",
		h4: "mdc-typography--subtitle1",
		h5: "mdc-typography--subtitle2",
		h6: "mdc-typography--overline",
		p: "mdc-typography--body1"
	});

	// code highlighting
	markdownCompiler.use(markdownItPrism);

	// run the markdown compiler and render markdown body as html
	let html = markdownCompiler.render(body);

	// inject the require()s for loading images
	if (hasImagesToRequire) {
		/**
		 * We convert this:
		 *
		 * '<img src="${{{./image.png}}} alt="image" />'
		 *
		 * into this:
		 *
		 * [
		 *     '<img src="',
		 *     require("./image.png").default,
		 *     '" alt="image" />'
		 * ].join("");
		 *
		 * so the file-loader can handle the require()s to get us the output
		 * path for the img src.
		 */

		/**
		 * split the html string like this:
		 * [
		 *     '<img src="',
		 *     './image.png}}}" alt="image" />'
		 * ]
		 */
		html = html.split("${{{");

		// start our array string
		let arrayString = "[";

		// append the first chunk to it
		arrayString += stringify(html[0]);

		// for each except the first
		for (let i = 1, len = html.length; i < len; i++) {
			const cur = html[i],
				endIndex = html[i].indexOf("}}}");

			/**
			 * turn this: './image.png}}}" alt="image" />'
			 * into this: [ require("./image.png").default,'" alt="image" />' ]
			 *
			 * then append it to our string array
			 */
			arrayString += `,require("${cur.slice(
				0,
				endIndex
			)}").default,${stringify(cur.slice(endIndex + 3))}`;
		}

		// return our module with the string array
		return `${exported}${arrayString}].join("")};`;
	}

	// return module
	return `${exported}${stringify(html)}};`;
}

// webpack loader entry point
module.exports = function(source) {
	const callback = this.async();

	if (this.cacheable) this.cacheable();

	// wrap the main async function
	processMarkdown(source)
		.then(result => {
			callback(null, result);
		})
		.catch(err => {
			callback(err);
		});
};

module.exports = {
	parser: "babel-eslint",
	parserOptions: {
		ecmaVersion: 2019,
		sourceType: "module"
	},
	env: {
		es6: true,
		browser: true
	},
	plugins: ["svelte3"],
	extends: ["eslint:recommended"],
	overrides: [
		{
			files: ["**/*.svelte"],
			processor: "svelte3/svelte3"
		}
	],
	ignorePatterns: ["build/", "config/", "scripts/"],
	rules: {
		"sort-imports": "warn"
	},
	settings: {
		"svelte3/ignore-styles": input =>
			input.type && input.type !== "text/css"
	}
};

/* eslint-disable */
require("dotenv").config();
import HerokuClient from "heroku-client";

const heroku = new HerokuClient({ token: process.env.HEROKU_API_TOKEN });

export async function handler(event, context) {
	const app = event.queryStringParameters.app;

	if (!app || !/^[a-z0-9\-]+$/i.test(app)) {
		return {
			statusCode: 400,
			body: JSON.stringify({
				message: "invalid app name"
			})
		};
	}

	try {
		const res = await heroku.get(`/apps/${app}/dynos`);
		const { state, updated_at } = res[0];
		return {
			statusCode: 200,
			body: JSON.stringify({
				state,
				updated_at
			})
		};
	} catch (error) {
		return {
			statusCode: error.statusCode || 403,
			body: JSON.stringify(
				error.body || {
					message: "internal error"
				}
			)
		};
	}
}

/* eslint-disable */
import https from "https";

let queuedTimeout;

function fetch(url, timeout = 2000) {
	return new Promise((resolve, reject) => {
		clearTimeout(queuedTimeout);

		const request = https
			.get(url, res => {
				let data = "";
				res.on("data", chunk => {
					data += chunk;
				});
				res.on("end", () => {
					resolve(data);
				});
			})
			.on("error", err => {
				reject(err);
			});

		queuedTimeout = setTimeout(() => {
			request.abort();
			reject("timedout");
		}, timeout);
	});
}

export async function handler(event, context) {
	const app = event.queryStringParameters.app;

	if (!app || !/^[a-z0-9\-]+$/i.test(app)) {
		return {
			statusCode: 400,
			body: JSON.stringify({
				message: "invalid app name"
			})
		};
	}

	try {
		// await fetch(`https://httpbin.org/delay/10`);
		await fetch(`https://${app}.herokuapp.com`);
	} catch (error) {
		console.error(error);
		return {
			statusCode: 403,
			body: JSON.stringify({
				message: error === "timedout" ? "timedout" : "internal error"
			})
		};
	}

	return {
		statusCode: 200,
		body: JSON.stringify({ message: "hit" })
	};
}

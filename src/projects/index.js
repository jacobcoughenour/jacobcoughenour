import importAll from "import-all.macro";

// import all the project page files
const projectPageFiles = importAll.sync("./**/*.md");
const regex = /([^/]+)((?=\/index\.md+$)|(?=\.md+$))/;

export const projectsById = {};

export const projects = Object.keys(projectPageFiles)
	.map(path => {
		const id = regex.exec(path)[0];
		const { html, attributes } = projectPageFiles[path];

		attributes.created_at = attributes.created_at
			? new Date(attributes.created_at)
			: 0;
		attributes.last_activity_at = attributes.last_activity_at
			? new Date(attributes.last_activity_at)
			: 0;

		return (projectsById[id] = {
			...attributes,
			id,
			html
		});
	})
	.sort((a, b) => b.last_activity_at - a.last_activity_at);

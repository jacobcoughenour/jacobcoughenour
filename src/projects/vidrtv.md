---
gitlab_id: 9393269
homepage: "https://vidr.herokuapp.com/"
description: "A video sharing platform"
tools:
 - react
 - webpack
 - babel
 - eslint
 - express
 - gulp
 - apollo
 - MongoDB
 - Socket.io
 - mustache
 - prettier
 - Material UI
---
### title

Nihil vero reiciendis in error ut provident. Alias pariatur temporibus consequatur alias dicta magni sint quis. Laborum excepturi et et voluptas accusamus aspernatur et illo. Et sint sunt occaecati saepe. Fugit eius dolor quia provident velit eos quia eos. Corrupti minus temporibus aut.

Enim qui quia atque necessitatibus magnam est amet. Ab voluptatibus fugit consequatur. Exercitationem rem itaque libero dolorem. Aut soluta voluptatibus aut nulla veritatis dolores. Eveniet libero consequuntur optio non doloribus unde hic.

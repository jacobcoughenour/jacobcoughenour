---
gitlab_id: 7769361
homepage: https://pluginiconcreator.netlify.com/
description: uMod plugin icon creator
tools:
 - PixiJS
 - WebGL
 - jQuery
 - Bootstrap
---

###### screenshots
-----
- ![screenshot](screenshot.png)
- ![test](https://i.imgur.com/rr9yxoT.png)
{data=image-collection}

# h1
---

## h2
### h3
#### h4
##### h5
###### h6

```js
// fetch project language stats
let languages = await fetch(projectRoute + "/languages").then(res =>
    res.json()
);

// make the language keys lower case
languages = Object.keys(languages).reduce(
    (a, key) => ((a[key.toLowerCase()] = languages[key]), a),
    {}
);
```

This project was one of my contributions to the uMod community.


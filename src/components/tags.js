import LanguageColors from "language-colors";
import importAll from "import-all.macro";

// import all the logo svg files
const logoFiles = importAll.sync("../logos/*.svg");
const logos = {};
const regex = /([^/]+)(?=\.\w+$)/;

// maps the default export to each filename
Object.keys(logoFiles).forEach(
	path => (logos[regex.exec(path)[0]] = logoFiles[path].default)
);

// tag property overrides
const tags = {
	css: {
		label: "CSS",
		color: "#29A7DD"
	},
	html: {
		label: "HTML"
	},
	graphql: {
		label: "GraphQL"
	},
	eslint: {
		label: "ESLint"
	},
	glsl: {
		label: "GLSL",
		color: "#009688"
	},
	lua: {
		color: "#4747d4"
	}
};

export function getColor(tag) {
	if (tags[tag] && tags[tag].color) return tags[tag].color;
	if (LanguageColors[tag]) return LanguageColors[tag];
}

export function getLabel(tag) {
	if (tags[tag] && tags[tag].label) return tags[tag].label;
	return tag.charAt(0).toUpperCase() + tag.slice(1);
}

export function hasIcon(tag) {
	return !!logos[tag.toLowerCase()];
}

export function getIcon(tag) {
	const icon = logos[tag.toLowerCase()];
	return `<svg viewBox="${icon.viewBox}"><use fill="white" xlink:href="#${icon.id}"/></svg>`;
}

export function getLink(tag) {
	if (tags[tag] && tags[tag].link) return tags[tag].link;
	return `https://duckduckgo.com/?q=${tag}`;
}

uniform vec3 diffuse;
uniform vec3 colors[16];
uniform float opacity;
uniform float time;
uniform float totalDistances[16];
uniform float linewidth;
// #ifdef USE_DASH
	uniform float dashSize;
	uniform float gapSize;
// #endif
varying float vLineDistance;
varying float vLineIndex;

#include <common>
#include <color_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>

varying vec2 vUv;

vec3 getColor(int index) {
	for ( int i = 0; i < 16; i++ ) {
		if ( i == index ) return colors[i];
	}
	return colors[0];
}

float getDistance(int index) {
	for ( int i = 0; i < 16; i++ ) {
		if ( i == index ) return totalDistances[i];
	}
	return 1.0;
}

void main() {
	#include <clipping_planes_fragment>

	// #ifdef USE_DASH
	// 	if ( vUv.y < - 1.0 || vUv.y > 1.0 ) discard; // discard endcaps
	// 	if ( mod( vLineDistance, dashSize + gapSize ) > dashSize ) discard; // todo - FIX
	// #endif

	int index = int(vLineIndex);

	// offset the time based on line index
	float offtime = time - vLineIndex * 0.824;

	// wait until it's our turn to start
	if (offtime < 0.0)
		discard;

	float dist = getDistance(index);
	float extra = 1.0;
	float totalmod = mod(offtime, dist * 2.0 + extra * 2.0);
	float distfromend;

	if (totalmod < dist + extra || totalmod > dist * 2.0 + extra * 4.0)
		distfromend = totalmod - vLineDistance;
	else
		distfromend = vLineDistance - mod(offtime, dist + extra) + extra * 2.0;

	if (distfromend < 0.75) {

		float halfwidth = float(linewidth) * 0.5;

		float a = (3.0 / halfwidth) - distfromend + 0.25;
		a *= halfwidth;

		float b = ( vUv.y > 0.0 ) ? vUv.y - 1.0 : vUv.y + 1.0;

		if ( vUv.x * vUv.x + a * a > 1.0 || (abs( vUv.y ) > 1.0 && vUv.x * vUv.x + b * b > 1.0) )
			discard;
	}
	else {
		// rounded endcaps
		if ( abs( vUv.y ) > 1.0 ) {
			float b = ( vUv.y > 0.0 ) ? vUv.y - 1.0 : vUv.y + 1.0;
			if ( vUv.x * vUv.x + b * b > 1.0 )
				discard;
			// distfromend *= 0.5;
		}
	}

	// if (distfromend < 0.0)
		// discard;

	// distfromend = pow(distfromend + 0.5, 6.0);



	// vec4 diffuseColor = vec4( getColor(index).rgb, max(0.0, distfromend) );
	vec4 diffuseColor = vec4( getColor(index).rgb, opacity );

	#include <logdepthbuf_fragment>
	#include <color_fragment>

	gl_FragColor = vec4( diffuseColor.rgb, diffuseColor.a );

	#include <premultiplied_alpha_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
}
import { ShaderMaterial, UniformsLib, UniformsUtils, Vector2 } from "three";

import fragment from "./line.frag";
import vertex from "./line.vert";

const uniforms = UniformsUtils.merge([
	UniformsLib.common,
	UniformsLib.fog,
	{
		time: { type: "f", value: 1.0 },
		linewidth: { value: 1 },
		resolution: { value: new Vector2(1, 1) },
		dashScale: { value: 1 },
		dashSize: { value: 0 },
		gapSize: { value: 0 },
		colors: { type: "3fv", value: [0, 1, 0] },
		totalDistances: { type: "fv", value: [0] }
	}
]);

export default class LineMaterial extends ShaderMaterial {
	type = "LineMaterial";

	constructor(props) {
		super({
			type: "LineMaterial",
			uniforms: UniformsUtils.clone(uniforms),
			fog: true,
			transparent: true,
			vertexShader: vertex,
			fragmentShader: fragment,
			depthWrite: true
		});

		this.isLineMaterial = true;
		this.dashed = false;

		Object.defineProperties(this, {
			color: {
				enumerable: true,
				get: () => this.uniforms.diffuse.value,
				set: value => (this.uniforms.diffuse.value = value)
			},
			colors: {
				enumerable: true,
				get: () => this.uniforms.colors.value,
				set: value => (this.uniforms.colors.value = value)
			},
			linewidth: {
				enumerable: true,
				get: () => this.uniforms.linewidth.value,
				set: value => (this.uniforms.linewidth.value = value)
			},
			dashed: {
				enumerable: true,
				get: () => this.defines.USE_DASH === "",
				set: value => {
					if (value) this.defines.USE_DASH = "";
					else delete this.defines.USE_DASH;
				}
			},
			dashScale: {
				enumerable: true,
				get: () => this.uniforms.dashScale.value,
				set: value => (this.uniforms.dashScale.value = value)
			},
			dashSize: {
				enumerable: true,
				get: () => this.uniforms.dashSize.value,
				set: value => (this.uniforms.dashSize.value = value)
			},
			gapSize: {
				enumerable: true,
				get: () => this.uniforms.gapSize.value,
				set: value => (this.uniforms.gapSize.value = value)
			},
			resolution: {
				enumerable: true,
				get: () => this.uniforms.resolution.value,
				set: value => this.uniforms.resolution.value.copy(value)
			}
		});

		this.setValues(props);
	}
}

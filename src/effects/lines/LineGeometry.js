import { InstancedInterleavedBuffer, InterleavedBufferAttribute } from "three";
import LineSegmentsGeometry from "./LineSegmentsGeometry";

export default class LineGeometry extends LineSegmentsGeometry {
	constructor() {
		super();
		this.type = "LineGeometry";

		this.totalDistances = [];
	}

	isLineGeometry = true;

	fromLines(lines) {
		this.totalDistances = [];

		const totalpoints = lines.reduce((a, e) => a + e.length, 0) / 3;

		const totalsegments = totalpoints - lines.length;

		// segment buffer
		const segmentpoints = new Int16Array(totalsegments * 6);
		// line index buffer
		const indexbuffer = new Uint8Array(totalsegments * 2);
		// segment distance buffer
		const distances = new Uint16Array(totalsegments * 2);

		for (
			let lineIndex = 0, segoffset = 0;
			lineIndex < lines.length;
			lineIndex++
		) {
			const line = lines[lineIndex];

			// fill line index buffer
			// index.fill(lineIndex, offset, offset + lines[lineIndex].length / 3);
			// first index
			// indexbuffer[offset] = lineIndex;

			// number of segments for current line
			const len = line.length / 3 - 1;

			// for each point - 1
			for (let j = 0; j < len; j++) {
				const // segment start point
					startx = line[j * 3],
					starty = line[j * 3 + 1],
					startz = line[j * 3 + 2],
					// segment end point
					endx = line[j * 3 + 3],
					endy = line[j * 3 + 4],
					endz = line[j * 3 + 5];

				// push start and end to segment buffer
				// [sx1, sy1, sz1, ex1, ey1, ez1, sx2, sy2 ...]
				{
					// current index offset for the segment buffer
					const off = (segoffset + j) * 6;
					segmentpoints[off] = startx;
					segmentpoints[off + 1] = starty;
					segmentpoints[off + 2] = startz;
					segmentpoints[off + 3] = endx;
					segmentpoints[off + 4] = endy;
					segmentpoints[off + 5] = endz;
				}

				// index buffer
				indexbuffer[(segoffset + j) * 2] = lineIndex % 16;
				indexbuffer[(segoffset + j) * 2 + 1] = lineIndex % 16;

				// distance buffer
				{
					// current index offset for the distance buffer
					const off = (segoffset + j) * 2;

					// use last end distance as our starting distance
					// this is 0 if there is no previous connected segment
					const prev = j === 0 ? 0 : distances[off - 1];
					distances[off] = prev;

					// calculate distance between start and end of this segment
					const dx = startx - endx,
						dy = starty - endy,
						dz = startz - endz;
					distances[off + 1] =
						prev + Math.sqrt(dx * dx + dy * dy + dz * dz);
				}
			}

			{
				const dist = distances[(segoffset + len) * 2 - 1];
				if (lineIndex < 16) this.totalDistances.push(dist);
				else {
					// if we are sharing an index, pick the larger distance
					this.totalDistances[lineIndex % 16] = Math.max(
						this.totalDistances[lineIndex % 16],
						dist
					);
				}
			}

			// current position in segmentpoints array
			segoffset += len;

			// current total point index
			// offset += line.length / 3;
		}

		const interleavedBuffer = new InstancedInterleavedBuffer(
			segmentpoints,
			6,
			1
		);

		this.setAttribute(
			"instanceStart",
			new InterleavedBufferAttribute(interleavedBuffer, 3, 0)
		);
		this.setAttribute(
			"instanceEnd",
			new InterleavedBufferAttribute(interleavedBuffer, 3, 3)
		);

		const distanceBuffer = new InstancedInterleavedBuffer(distances, 2, 1);

		this.setAttribute(
			"instanceDistanceStart",
			new InterleavedBufferAttribute(distanceBuffer, 1, 0)
		);
		this.setAttribute(
			"instanceDistanceEnd",
			new InterleavedBufferAttribute(distanceBuffer, 1, 1)
		);

		const colorBuffer = new InstancedInterleavedBuffer(indexbuffer, 2, 1);

		this.setAttribute(
			"instanceIndexStart",
			new InterleavedBufferAttribute(colorBuffer, 1, 0)
		);
		this.setAttribute(
			"instanceIndexEnd",
			new InterleavedBufferAttribute(colorBuffer, 1, 1)
		);

		this.computeBoundingBox();
		this.computeBoundingSphere();

		return this;
	}

	setPositions(array) {
		// convert points into pairs
		const points = new Float32Array((array.length - 3) * 2);

		for (let i = 0; i < array.length - 3; i += 3)
			for (let j = 0; j < 6; j++) points[i * 2 + j] = array[i + j];

		super.setPositions(points);

		return this;
	}

	setColors(array) {
		array;
		return this;
	}
}

import LineMaterial from "../materials/LineMaterial";
import LineSegmentsGeometry from "./LineSegmentsGeometry";
import { Mesh } from "three";

export default class LineSegments extends Mesh {
	type = "LineSegments";

	constructor(
		geometry = new LineSegmentsGeometry(),
		material = new LineMaterial()
	) {
		super(geometry, material);

		this.material.uniforms.totalDistances.value = geometry.totalDistances;
		this.material.needsUpdate = true;
	}
}

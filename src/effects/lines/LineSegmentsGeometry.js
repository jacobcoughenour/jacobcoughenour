import {
	Box3,
	Float32BufferAttribute,
	InstancedBufferGeometry,
	InstancedInterleavedBuffer,
	InterleavedBufferAttribute,
	Sphere,
	Vector3
} from "three";

export default class LineSegmentsGeometry extends InstancedBufferGeometry {
	constructor() {
		super();

		this.type = "LineSegmentsGeometry";

		//prettier-ignore
		var positions = [
			-1, 2, 0,
			1, 2, 0,
			-1, 1, 0,
			1, 1, 0,
			-1, 0, 0,
			1, 0, 0,
			-1, -1, 0,
			1, -1, 0
		];
		var uvs = [-1, 2, 1, 2, -1, 1, 1, 1, -1, -1, 1, -1, -1, -2, 1, -2];
		var index = [0, 2, 1, 2, 3, 1, 2, 4, 3, 4, 5, 3, 4, 6, 5, 6, 7, 5];

		this.setIndex(index);
		this.setAttribute("position", new Float32BufferAttribute(positions, 3));
		this.setAttribute("uv", new Float32BufferAttribute(uvs, 2));
	}

	static isLineSegmentsGeometry = true;

	applyMatrix(matrix) {
		const { instanceStart, instanceEnd } = this.attributes;

		if (instanceStart !== undefined) {
			matrix.applyToBufferAttribute(instanceStart);
			matrix.applyToBufferAttribute(instanceEnd);

			instanceStart.data.needsUpdate = true;
		}

		if (this.boundingBox !== null) this.computeBoundingBox();

		if (this.boundingSphere !== null) this.computeBoundingSphere();
	}

	setPositions(array) {
		if (Array.isArray(array)) array = new Float32Array(array);

		const interleavedBuffer = new InstancedInterleavedBuffer(array, 6, 1);

		this.setAttribute(
			"instanceStart",
			new InterleavedBufferAttribute(interleavedBuffer, 3, 0)
		);
		this.setAttribute(
			"instanceEnd",
			new InterleavedBufferAttribute(interleavedBuffer, 3, 3)
		);

		this.computeBoundingBox();
		this.computeBoundingSphere();

		return this;
	}

	computeBoundingBox() {
		if (!this.boundingBox) this.boundingBox = new Box3();

		const { instanceStart, instanceEnd } = this.attributes;

		if (instanceStart && instanceEnd) {
			this.boundingBox.setFromBufferAttribute(instanceStart);

			const box = new Box3();
			box.setFromBufferAttribute(instanceEnd);

			this.boundingBox.union(box);
		}
	}

	computeBoundingSphere() {
		if (!this.boundingSphere) this.boundingSphere = new Sphere();

		if (!this.boundingBox) this.computeBoundingBox();

		const { instanceStart, instanceEnd } = this.attributes;

		if (instanceStart && instanceEnd) {
			// use the vec3 already created by bounding sphere
			// if we did ..center = ..getCenter(), it would create another vec3
			const center = this.boundingSphere.center;

			this.boundingBox.getCenter(center);

			let max = 0;
			const vec = new Vector3();

			for (let i = 0, il = instanceStart.count; i < il; i++) {
				vec.fromBufferAttribute(instanceStart, i);
				max = Math.max(max, center.distanceToSquared(vec));

				vec.fromBufferAttribute(instanceEnd, i);
				max = Math.max(max, center.distanceToSquared(vec));
			}

			this.boundingSphere.radius = Math.sqrt(max);
		}
	}
}

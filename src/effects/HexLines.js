import * as THREE from "three";

import Line from "./lines/Line";
import LineGeometry from "./lines/LineGeometry";
import LineMaterial from "./materials/LineMaterial";

// color palette to randomly choose from
// limited to 16 since this array gets passed to
const colors = [
	0xff1744,
	0xf50057,
	0xd500f9,
	0x651fff,
	0x3d5afe,
	0x2979ff,
	0x00b0ff,
	0x00e5ff,
	0x1de9b6,
	0x00e676,
	0x76ff03,
	0xc6ff00,
	0xffea00,
	0xffc400,
	0xff9100,
	0xff3d00
];

export default class HexLines extends Line {
	constructor(size = 40, num = 32, maxlength = 8) {
		const generator = new LineGenerator(size, maxlength);
		const geometry = new LineGeometry();

		geometry.fromLines(generator.generate(num));
		geometry.setColors(colors);

		super(
			geometry,
			new LineMaterial({
				colors: colors.reduce((a, hex) => {
					a.push(
						((hex >> 16) & 255) / 255,
						((hex >> 8) & 255) / 255,
						(hex & 255) / 255
					);
					return a;
				}, []),
				linewidth: 12,
				// vertexColor: THREE.VertexColors,
				resolution: new THREE.Vector2(
					window.innerWidth,
					window.innerHeight
				)
			})
		);

		if (!size.length) {
			this.position.set(-size / 2, -size / 2, -size / 2);
		} else if (size.length === 2) {
			this.position.set(
				(-size[0] + size[1]) / 4,
				-size[1] / 2,
				(size[0] + size[1]) / 4
			);
		} else this.position.set(-size[0] / 2, -size[1] / 2, -size[2] / 2);
	}

	animate(time) {
		this.material.uniforms.time.value = time * 0.002;
	}
}

// line geometry generator
class LineGenerator {
	constructor(size = [40, 40], maxlength = 8) {
		if (!size.length) size = [size, size, size];

		// max dimensions [width, height]
		this.size = size;
		// max line length
		this.maxlength = maxlength;

		// create our 2d matrix
		// only used for checking collisions during generation
		this.matrix =
			size.length === 2
				? new Array(size[0])
						.fill(1)
						.map(() => new Array(size[1]).fill(false))
				: new Array(size[0])
						.fill(1)
						.map(() =>
							new Array(size[1])
								.fill(1)
								.map(() => new Array(size[2]).fill(false))
						);

		/**
		 * short-hand for getting value at the given point in matrix
		 */
		this.get =
			size.length === 2
				? p => this.matrix[p[0]][p[1]]
				: p => this.matrix[p[0]][p[1]][p[2]];

		/**
		 * short-hand for setting value at the given point in matrix
		 */
		this.set =
			size.length === 2
				? (p, v) => (this.matrix[p[0]][p[1]] = v)
				: (p, v) => (this.matrix[p[0]][p[1]][p[2]] = v);
	}

	/**
	 * converts 2d point to 3d point on hex grid
	 */
	static toHex = (x, y) => {
		let d = y % 2 === 0 ? Math.ceil(y / 2) : Math.floor(y / 2);
		return y % 2 === 0
			? [Math.ceil(x / 2) - d, y, -Math.floor(x / 2) - d]
			: [Math.floor(x / 2) - d, y, -Math.ceil(x / 2) - d];
	};

	// four cardinal direction vectors
	// [up, left, right, down]
	static _hexdirs = [
		[0, 1],
		[-1, 0],
		[1, 0],
		[0, -1]
	];

	/**
	 * generate next point for the line given the previous point
	 * @param {number[]} p previous point
	 * @returns next point or false if there is no where left to go
	 */
	getNextPointHex(p) {
		// generate random num
		const rand = Math.floor(Math.random() * 3);

		// While there are 4 possible directions for each point on a 2d grid,
		// there are only 3 on a hex grid. So for "every other" coord (like a
		// checkerboard pattern), we alternate between [up, left, right] and
		// [left, right, down]
		const offset = p[0] % 2 === p[1] % 2 ? 0 : 1;

		// find a direction that we can move
		for (let i = 0, x = 0, y = 0, dir; i < 3; i++) {
			// get direction
			dir = LineGenerator._hexdirs[offset + ((rand + i) % 3)];

			// transform point in direction
			x = p[0] + dir[0];
			y = p[1] + dir[1];

			// loop if out of bounds
			if (x < 0 || y < 0 || x >= this.size[0] || y >= this.size[1])
				continue;

			// if point in matrix is empty, return it
			if (!this.get([x, y])) return [x, y];
		}

		// we tried all directions and none are valid
		// so return false
		return false;
	}

	static _dirs = [
		[1, 0, 0],
		[-1, 0, 0],
		[0, 1, 0],
		[0, -1, 0],
		[0, 0, 1],
		[0, 0, -1]
	];

	/**
	 * generate next point for the line given the previous point
	 * @param {number[]} p previous 2 points
	 * @returns next point or false if there is no where left to go
	 */
	getNextPoint(p) {
		// generate random num
		const rand = Math.floor(Math.random() * 6);

		// try continuing in the same direction
		if (p.length > 3 && Math.random() < 0.6) {
			const dir = [p[3] - p[0], p[4] - p[1], p[5] - p[2]];

			const x = p[3] + dir[0];
			const y = p[4] + dir[1];
			const z = p[5] + dir[2];

			// loop if out of bounds
			if (
				x >= 0 &&
				y >= 0 &&
				z >= 0 &&
				x < this.size[0] &&
				y < this.size[1] &&
				z < this.size[2]
			)
				if (!this.get([x, y, z]))
					// if point in matrix is empty, return it
					return [x, y, z];
		}

		const prev = p.slice(p.length - 3);

		// find a direction that we can move
		for (let i = 0, x, y, z, dir; i < 6; i++) {
			// get direction
			dir = LineGenerator._dirs[(rand + i) % 6];

			// transform point in direction

			x = prev[0] + dir[0];
			y = prev[1] + dir[1];
			z = prev[2] + dir[2];

			// loop if out of bounds
			if (
				x < 0 ||
				y < 0 ||
				z < 0 ||
				x >= this.size[0] ||
				y >= this.size[1] ||
				z >= this.size[2]
			)
				continue;

			// if point in matrix is empty, return it
			if (!this.get([x, y, z])) return [x, y, z];
		}

		// we tried all directions and none are valid
		// so return false
		return false;
	}

	/**
	 * generate the given number of lines
	 * @param {*} num how many to generate
	 * @returns generated lines
	 */
	generate(num) {
		const lines = [];
		const last = [];

		let sizelen = this.size.length;
		const isHex = sizelen === 2;

		// generate a starting point for each line
		for (let i = 0; i < num; i++) {
			let point = isHex ? [0, 0] : [0, 0, 0],
				tries = 0;
			do {
				if (!isHex)
					point[2] =
						Math.floor((this.size[2] * Math.random()) / 2) * 2;
				point[1] = Math.floor((this.size[1] * Math.random()) / 2) * 2;
				point[0] = Math.floor((this.size[0] * Math.random()) / 2) * 2;
				tries++;
			} while (!!this.get(point) && tries < num);

			lines.push(isHex ? LineGenerator.toHex(...point) : point);
			last.push(point);
			this.set(point, i + 1);
		}

		let len = 0;
		let active = new Array(num).fill(true);
		let c = active.length;

		// genate each line in parelel
		while (c > 0 && len < this.maxlength) {
			c = 0;
			for (let i = 0; i < num; i++) {
				if (!active[i]) continue;

				const next = isHex
					? this.getNextPointHex(last[i].slice(last[i].length - 2))
					: this.getNextPoint(last[i].slice(last[i].length - 6));

				if (
					!next ||
					Math.floor(Math.random() * this.maxlength * 2) === 0
				) {
					active[i] = false;
					break;
				}

				lines[i] = lines[i].concat(
					isHex ? LineGenerator.toHex(...next) : next
				);
				last[i] = last[i].concat(next);

				this.set(next, i + 1);
				c++;
			}
			len++;
		}

		return lines;
	}
}

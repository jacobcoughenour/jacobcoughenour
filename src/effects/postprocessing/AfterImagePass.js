import {
	LinearFilter,
	MeshBasicMaterial,
	NearestFilter,
	RGBAFormat,
	ShaderMaterial,
	UniformsUtils,
	WebGLRenderTarget
} from "three";
import Pass, { FullScreenQuad } from "./Pass";

import fragmentShader from "./afterimage.frag";
import vertexShader from "./default.vert";

const AfterImageShader = {
	uniforms: {
		damp: { value: 0.96 },
		tOld: { value: null },
		tNew: { value: null }
	},
	vertexShader,
	fragmentShader
};

export default class AfterImagePass extends Pass {
	constructor(damp, width, height) {
		super();

		this.shader = AfterImageShader;

		this.uniforms = UniformsUtils.clone(AfterImageShader.uniforms);
		if (damp) this.uniforms["damp"].value = damp;

		this.textureComp = new WebGLRenderTarget(width, height, {
			minFilter: LinearFilter,
			magFilter: NearestFilter,
			format: RGBAFormat
		});

		this.textureOld = this.textureComp.clone();

		this.shaderMaterial = new ShaderMaterial({
			uniforms: this.uniforms,
			vertexShader,
			fragmentShader
		});

		this.compFsQuad = new FullScreenQuad(this.shaderMaterial);
		this.copyFsQuad = new FullScreenQuad(new MeshBasicMaterial());
	}

	render(renderer, writeBuffer, readBuffer) {
		this.uniforms["tOld"].value = this.textureOld.texture;
		this.uniforms["tNew"].value = readBuffer.texture;

		renderer.setRenderTarget(this.textureComp);
		this.compFsQuad.render(renderer);

		this.copyFsQuad.material.map = this.textureComp.texture;

		if (this.renderToScreen) {
			renderer.setRenderTarget(null);
			this.copyFsQuad.render(renderer);
		} else {
			renderer.setRenderTarget(writeBuffer);

			if (this.clear) renderer.clear();

			this.copyFsQuad.render(renderer);
		}

		const temp = this.textureOld;
		this.textureOld = this.textureComp;
		this.textureComp = temp;
	}

	setSize(width, height) {
		this.textureComp.setSize(width, height);
		this.textureOld.setSize(width, height);
	}
}

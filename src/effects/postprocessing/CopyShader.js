import fragmentShader from "./copy.frag";
import vertexShader from "./default.vert";

export default {
	uniforms: {
		tDiffuse: { value: null },
		opacity: { value: 1.0 }
	},
	vertexShader,
	fragmentShader
};

import { Vector2 } from "three";
import fragmentShader from "./test.frag";
import vertexShader from "./test.vert";

export default {
	uniforms: {
		tDiffuse: { type: "t", value: null },
		time: { type: "f", value: 1.0 },
		resolution: { value: new Vector2(1, 1) }
	},
	vertexShader,
	fragmentShader
};

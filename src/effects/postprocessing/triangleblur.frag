uniform sampler2D texture;
uniform vec2 delta;
uniform vec4 rect;
varying vec2 vUv;

float random(vec3 scale, float seed) {
	return fract(sin(dot(gl_FragCoord.xyz + seed, scale)) * 43758.5453 + seed);
}

void main() {
	if ( vUv.x < rect.x || vUv.x > rect.z || vUv.y < rect.y || vUv.y > rect.w ) {
		gl_FragColor = texture2D(texture, vUv);
		// gl_FragColor = vec4(1.0);
		return;
	}

	vec4 color = vec4(0.0);
	float total = 0.0;

	float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);

	for ( float t = -ITERATIONS; t <= ITERATIONS; t++ ) {
		float percent = (t + offset - 0.5) / ITERATIONS;
		float weight = 1.0 - abs(percent);

		vec4 sample = texture2D(texture, vUv + delta * percent);

		// sample.rgb *= sample.a;

		color += sample * weight;
		total += weight;
	}

	// desaturate
	// float avg = (color.r + color.g + color.b) / 3.0;
	// color = vec4(mix(color.rgb, vec3(avg), max(0.0, avg * 0.1)), color.a);

	// gl_FragColor = texture2D(texture, vUv);
	gl_FragColor = color / total;
}
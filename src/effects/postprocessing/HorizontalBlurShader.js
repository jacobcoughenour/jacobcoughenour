import fragmentShader from "./hblur.frag";
import vertexShader from "./default.vert";

export default {
	uniforms: {
		tDiffuse: { type: "t", value: null },
		h: { type: "f", value: 1.0 / 512.0 }
	},
	vertexShader,
	fragmentShader
};

import {
	Color,
	LinearFilter,
	MeshDepthMaterial,
	NoBlending,
	RGBADepthPacking,
	RGBFormat,
	ShaderMaterial,
	UniformsUtils,
	WebGLRenderTarget
} from "three";
import Pass, { FullScreenQuad } from "./Pass";

import fragmentShader from "./bokeh.frag";
import vertexShader from "./default.vert";

const BokehShader = {
	defines: {
		DEPTH_PACKING: 1,
		PERSPECTIVE_CAMERA: 0
	},
	uniforms: {
		tColor: { value: null },
		tDepth: { value: null },
		focus: { value: 1.0 },
		aspect: { value: 1.0 },
		aperture: { value: 0.025 },
		maxblur: { value: 1.0 },
		nearClip: { value: 1.0 },
		farClip: { value: 1000.0 }
	},
	vertexShader,
	fragmentShader
};

export default class BokehPass extends Pass {
	constructor(scene, camera, params) {
		super();

		this.scene = scene;
		this.camera = camera;

		const focus = params.focus || 1.0;
		const aspect = params.aspect || camera.aspect;
		const aperture = params.aperture || 0.025;
		const maxblur = params.maxblur || 1.0;

		// render targets

		const width = params.width || window.innerWidth || 1;
		const height = params.height || window.innerHeight || 1;

		this.renderTargetColor = new WebGLRenderTarget(width, height, {
			minFilter: LinearFilter,
			magFilter: LinearFilter,
			format: RGBFormat
		});
		this.renderTargetColor.texture.name = "BokehPass.color";

		this.renderTargetDepth = this.renderTargetColor.clone();
		this.renderTargetDepth.texture.name = "BokehPass.depth";

		// depth material

		this.materialDepth = new MeshDepthMaterial();
		this.materialDepth.depthPacking = RGBADepthPacking;
		this.materialDepth.blending = NoBlending;

		// bokeh material

		const uniforms = UniformsUtils.clone(BokehShader.uniforms);

		uniforms["tDepth"].value = this.renderTargetDepth.texture;

		uniforms["focus"].value = focus;
		uniforms["aspect"].value = aspect;
		uniforms["aperture"].value = aperture;
		uniforms["maxblur"].value = maxblur;
		uniforms["nearClip"].value = camera.near;
		uniforms["farClip"].value = camera.far;

		this.materialBokeh = new ShaderMaterial({
			defines: Object.assign({}, BokehShader.defines),
			uniforms,
			vertexShader: BokehShader.vertexShader,
			fragmentShader: BokehShader.fragmentShader
		});

		this.uniforms = uniforms;
		this.needsSwap = false;

		this.fsQuad = new FullScreenQuad(this.materialBokeh);

		this.oldClearColor = new Color();
	}

	render(renderer, writeBuffer, readBuffer) {
		// render depth into texture

		this.scene.overrideMaterial = this.materialDepth;

		this.oldClearColor.copy(renderer.getClearColor());
		const oldClearAlpha = renderer.getClearAlpha();
		const oldAutoClear = renderer.autoClear;
		renderer.autoClear = false;

		renderer.setClearColor(0xffffff);
		renderer.setClearAlpha(1.0);
		renderer.setRenderTarget(this.renderTargetDepth);
		renderer.clear();
		renderer.render(this.scene, this.camera);

		// render bokeh composite

		this.uniforms["tColor"].value = readBuffer.texture;
		this.uniforms["nearClip"].value = this.camera.near;
		this.uniforms["farClip"].value = this.camera.far;

		if (this.renderToScreen) {
			renderer.setRenderTarget(null);
			this.fsQuad.render(renderer);
		} else {
			renderer.setRenderTarget(writeBuffer);
			renderer.clear();
			this.fsQuad.render(renderer);
		}

		this.scene.overrideMaterial = null;
		renderer.setClearColor(this.oldClearColor);
		renderer.setClearAlpha(oldClearAlpha);
		renderer.autoClear = oldAutoClear;
	}
}

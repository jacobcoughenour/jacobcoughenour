import { Vector2, Vector4 } from "three";

import fragmentShader from "./triangleblur.frag";
import vertexShader from "./default.vert";

export default {
	uniforms: {
		texture: { type: "t", value: null },
		delta: { type: "v2", value: new Vector2(1, 0) },
		rect: { type: "v4", value: new Vector4(0.5, 0.5, 0.9, 0.7) }
	},
	defines: {
		ITERATIONS: "8.0"
	},
	vertexShader,
	fragmentShader
};

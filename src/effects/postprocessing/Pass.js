import { Mesh, OrthographicCamera, PlaneBufferGeometry } from "three";

export default class Pass {
	enabled = true;
	needsSwap = true;
	clear = false;
	renderToScreen = false;

	setSize() {}
	render() {}
}

const camera = new OrthographicCamera(-1, 1, 1, -1, 0, 1);
const geometry = new PlaneBufferGeometry(2, 2);

export class FullScreenQuad {
	constructor(material) {
		this._mesh = new Mesh(geometry, material);
	}

	get material() {
		return this._mesh.material;
	}

	set material(value) {
		this._mesh.material = value;
	}

	render(renderer) {
		renderer.render(this._mesh, camera);
	}
}

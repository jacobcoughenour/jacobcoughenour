import {
	Clock,
	LinearFilter,
	RGBAFormat,
	Vector2,
	WebGLRenderTarget
} from "three";
import MaskPass, { ClearMaskPass } from "./MaskPass";

import CopyShader from "./CopyShader";
import ShaderPass from "./ShaderPass";

export default class EffectComposer {
	renderer;

	constructor(renderer, renderTarget = null) {
		this.renderer = renderer;

		// create a render target if none is provided
		if (!renderTarget) {
			this._pixelRatio = renderer.getPixelRatio();

			{
				const size = renderer.getSize(new Vector2());
				this._width = size.width;
				this._height = size.height;
			}

			renderTarget = new WebGLRenderTarget(
				this._width * this._pixelRatio,
				this._height * this._pixelRatio,
				{
					minFilter: LinearFilter,
					magFilter: LinearFilter,
					format: RGBAFormat,
					stencilBuffer: false
				}
			);

			renderTarget.texture.name = "EffectComposer.rt1";
		} else {
			this._pixelRatio = 1;
			this._width = renderTarget.width;
			this._height = renderTarget.height;
		}

		this.renderTarget1 = renderTarget;
		this.renderTarget2 = renderTarget.clone();

		this.renderTarget2.texture.name = "EffectComposer.rt2";

		this.writeBuffer = this.renderTarget1;
		this.readBuffer = this.renderTarget2;

		this.renderToScreen = true;

		this.passes = [];

		this.copyPass = new ShaderPass(CopyShader);

		this.clock = new Clock();
	}

	swapBuffers() {
		const temp = this.readBuffer;
		this.readBuffer = this.writeBuffer;
		this.writeBuffer = temp;
	}

	addPass(pass) {
		this.passes.push(pass);

		const size = this.renderer.getDrawingBufferSize(new Vector2());
		pass.setSize(size.width, size.height);
	}

	addPasses(passes) {
		this.passes = this.passes.concat(passes);

		const size = this.renderer.getDrawingBufferSize(new Vector2());
		passes.forEach(pass => pass.setSize(size.width, size.height));
	}

	insertPass(pass, index) {
		this.passes.splice(index, 0, pass);
	}

	isLastEnabledPass(passIndex) {
		if (passIndex >= this.passes.length - 1) return true;

		for (let i = passIndex + 1, len = this.passes.length; i < len; i++)
			if (this.passes[i].enabled) return false;

		return true;
	}

	render(deltaTime) {
		if (!deltaTime) deltaTime = this.clock.getDelta();

		const currentRenderTarget = this.renderer.getRenderTarget();

		let maskActive = false;

		const len = this.passes.length;

		for (let pass, i = 0; i < len; i++) {
			pass = this.passes[i];

			if (!pass.enabled) continue;

			pass.renderToScreen =
				this.renderToScreen && this.isLastEnabledPass(i);
			pass.render(
				this.renderer,
				this.writeBuffer,
				this.readBuffer,
				deltaTime,
				maskActive
			);

			if (pass.needsSwap) {
				if (maskActive) {
					const context = this.renderer.context;

					context.stencilFunc(context.NOTEQUAL, 1, 0xffffffff);

					this.copyPass.render(
						this.renderer,
						this.writeBuffer,
						this.readBuffer,
						deltaTime
					);

					context.stencilFunc(context.EQUAL, 1, 0xffffffff);
				}

				this.swapBuffers();
			}

			if (pass instanceof MaskPass) maskActive = true;
			else if (pass instanceof ClearMaskPass) maskActive = false;
		}

		this.renderer.setRenderTarget(currentRenderTarget);
	}

	reset(renderTarget) {
		// create a render target if none is provided
		if (!renderTarget) {
			this._pixelRatio = this.renderer.getPixelRatio();

			{
				const size = this.renderer.getSize(new Vector2());
				this._width = size.width;
				this._height = size.height;
			}

			renderTarget = this.renderTarget1.clone();
			renderTarget.setSize(
				this._width * this._pixelRatio,
				this._height * this._pixelRatio
			);
		}

		this.renderTarget1.dispose();
		this.renderTarget2.dispose();

		this.renderTarget1 = renderTarget;
		this.renderTarget2 = renderTarget.clone();

		this.writeBuffer = this.renderTarget1;
		this.readBuffer = this.renderTarget2;
	}

	setSize(width, height) {
		this._width = width;
		this._height = height;

		const effectiveWidth = width * this._pixelRatio;
		const effectiveHeight = height * this._pixelRatio;

		this.renderTarget1.setSize(effectiveWidth, effectiveHeight);
		this.renderTarget2.setSize(effectiveWidth, effectiveHeight);

		const len = this.passes.length;
		for (let i = 0; i < len; i++)
			this.passes[i].setSize(effectiveWidth, effectiveHeight);
	}

	setPixelRatio(pixelRatio) {
		this._pixelRatio = pixelRatio;
		this.setSize(this._width, this._height);
	}
}

import {
	AdditiveBlending,
	Color,
	LinearFilter,
	MeshBasicMaterial,
	RGBAFormat,
	ShaderMaterial,
	UniformsUtils,
	Vector2,
	Vector3,
	WebGLRenderTarget
} from "three";
import Pass, { FullScreenQuad } from "./Pass";

import CopyShader from "./CopyShader";
import compositeFragmentShader from "./composite.frag";
import fragmentShader from "./luminosityhigh.frag";
import separableblurFragmentShader from "./seperableblur.frag";
import vertexShader from "./default.vert";

const nMips = 5;

const LuminosityHighPassShader = {
	shaderID: "luminosityHighPass",
	uniforms: {
		tDiffuse: { value: null },
		luminosityThreshold: { value: 1.0 },
		smoothWidth: { value: 1.0 },
		defaultColor: { value: new Color(0x000000) },
		defaultOpacity: { value: 0.0 }
	},
	vertexShader,
	fragmentShader
};

export default class UnrealBloomPass extends Pass {
	static BlurDirectionX = new Vector2(1, 0);
	static BlurDirectionY = new Vector2(0, 1);

	constructor(
		resolution = new Vector2(256, 256),
		strength = 1,
		radius = 0,
		threshold = 0
	) {
		super();

		this.strength = strength;
		this.radius = radius;
		this.threshold = threshold;
		this.resolution = resolution;

		this.clearColor = new Color(0, 0, 0);

		this.renderTargetsHorizontal = [];
		this.renderTargetsVertical = [];

		let width = Math.round(this.resolution.x / 2);
		let height = Math.round(this.resolution.y / 2);
		const pars = {
			minFilter: LinearFilter,
			magFilter: LinearFilter,
			format: RGBAFormat
		};

		this.renderTargetBright = new WebGLRenderTarget(width, height, pars);
		this.renderTargetBright.texture.name = "UnrealBloomPass.bright";
		this.renderTargetBright.texture.generateMipmaps = false;

		for (let targetH, targetV, i = 0; i < nMips; i++) {
			targetH = new WebGLRenderTarget(width, height, pars);

			targetH.texture.name = `UnrealBloomPass.h${i}`;
			targetH.texture.generateMipmaps = false;

			this.renderTargetsHorizontal.push(targetH);

			targetV = new WebGLRenderTarget(width, height, pars);

			targetV.texture.name = `UnrealBloomPass.v${i}`;
			targetV.texture.generateMipmaps = false;

			this.renderTargetsVertical.push(targetV);

			width = Math.round(width / 2);
			height = Math.round(height / 2);
		}

		const highPassShader = LuminosityHighPassShader;
		this.highPassUniforms = UniformsUtils.clone(highPassShader.uniforms);

		this.highPassUniforms["luminosityThreshold"].value = threshold;
		this.highPassUniforms["smoothWidth"].value = 0.01;

		this.materialHighPassFilter = new ShaderMaterial({
			uniforms: this.highPassUniforms,
			vertexShader,
			fragmentShader,
			defines: {}
		});

		// gaussian blur materials
		this.separableBlurMaterials = [];
		const kernelSizeArray = [3, 5, 7, 9, 11];
		width = Math.round(this.resolution.x / 2);
		height = Math.round(this.resolution.y / 2);

		for (let i = 0; i < nMips; i++) {
			this.separableBlurMaterials.push(
				this.getSeperableBlurMaterial(kernelSizeArray[i])
			);
			this.separableBlurMaterials[i].uniforms[
				"texSize"
			].value = new Vector2(width, height);

			width = Math.round(width / 2);
			height = Math.round(height / 2);
		}

		// composite material
		this.compositeMaterial = this.getCompositeMaterial(nMips);
		{
			const { uniforms } = this.compositeMaterial;
			uniforms[
				"blurTexture1"
			].value = this.renderTargetsVertical[0].texture;
			uniforms[
				"blurTexture2"
			].value = this.renderTargetsVertical[1].texture;
			uniforms[
				"blurTexture3"
			].value = this.renderTargetsVertical[2].texture;
			uniforms[
				"blurTexture4"
			].value = this.renderTargetsVertical[3].texture;
			uniforms[
				"blurTexture5"
			].value = this.renderTargetsVertical[4].texture;
			uniforms["bloomStrength"].value = strength;
			uniforms["bloomRadius"].value = 0.1;
			this.compositeMaterial.needsUpdate = true;

			const bloomFactors = [1.0, 0.8, 0.6, 0.4, 0.2];
			uniforms["bloomFactors"].value = bloomFactors;
			this.bloomTintColors = [
				new Vector3(1, 1, 1),
				new Vector3(1, 1, 1),
				new Vector3(1, 1, 1),
				new Vector3(1, 1, 1),
				new Vector3(1, 1, 1)
			];
			uniforms["bloomTintColors"].value = this.bloomTintColors;
		}

		// copy material
		const copyShader = CopyShader;

		this.copyUniforms = UniformsUtils.clone(copyShader.uniforms);
		this.copyUniforms["opacity"].value = 1.0;

		this.materialCopy = new ShaderMaterial({
			uniforms: this.copyUniforms,
			vertexShader: copyShader.vertexShader,
			fragmentShader: copyShader.fragmentShader,
			blending: AdditiveBlending,
			depthTest: false,
			depthWrite: false,
			transparent: true
		});

		this.enabled = true;
		this.needsSwap = false;

		this.oldClearColor = new Color();
		this.oldClearAlpha = 1;

		this.basic = new MeshBasicMaterial();
		this.fsQuad = new FullScreenQuad(null);
	}

	dispose() {
		for (let i = 0; i < this.renderTargetsHorizontal.length; i++)
			this.renderTargetsHorizontal[i].dispose();
		for (let i = 0; i < this.renderTargetsVertical.length; i++)
			this.renderTargetsVertical[i].dispose();
		this.renderTargetBright.dispose();
	}

	setSize(width, height) {
		width = Math.round(width / 2);
		height = Math.round(height / 2);

		this.renderTargetBright.setSize(width, height);

		for (let i = 0; i < nMips; i++) {
			this.renderTargetsHorizontal[i].setSize(width, height);
			this.renderTargetsVertical[i].setSize(width, height);

			this.separableBlurMaterials[i].uniforms[
				"texSize"
			].value = new Vector2(width, height);

			width = Math.round(width / 2);
			height = Math.round(height / 2);
		}
	}

	render(renderer, writeBuffer, readBuffer, deltaTime, maskActive) {
		this.oldClearColor.copy(renderer.getClearColor());
		this.oldClearAlpha = renderer.getClearAlpha();

		const oldAutoClear = renderer.autoClear;
		renderer.autoClear = false;

		renderer.setClearColor(this.clearColor, 0);

		if (maskActive) renderer.context.disable(renderer.context.STENCIL_TEST);

		// render input to screen

		if (this.renderToScreen) {
			this.fsQuad.material = this.basic;
			this.basic.map = readBuffer.texture;

			renderer.setRenderTarget(null);
			renderer.clear();
			this.fsQuad.render(renderer);
		}

		// extract bright areas

		this.highPassUniforms["tDiffuse"].value = readBuffer.texture;
		this.highPassUniforms["luminosityThreshold"].value = this.threshold;
		this.fsQuad.material = this.materialHighPassFilter;

		renderer.setRenderTarget(this.renderTargetBright);
		renderer.clear();
		this.fsQuad.render(renderer);

		// blur all the mips progressively

		let inputRenderTarget = this.renderTargetBright;

		for (let sep, h, v, i = 0; i < nMips; i++) {
			sep = this.separableBlurMaterials[i];
			h = this.renderTargetsHorizontal[i];
			v = this.renderTargetsVertical[i];

			this.fsQuad.material = sep;
			sep.uniforms["colorTexture"].value = inputRenderTarget.texture;
			sep.uniforms["direction"].value = UnrealBloomPass.BlurDirectionX;

			renderer.setRenderTarget(h);
			renderer.clear();
			this.fsQuad.render(renderer);

			sep.uniforms["colorTexture"].value = h.texture;
			sep.uniforms["direction"].value = UnrealBloomPass.BlurDirectionY;

			renderer.setRenderTarget(v);
			renderer.clear();
			this.fsQuad.render(renderer);

			inputRenderTarget = v;
		}

		// composite all the mips

		this.fsQuad.material = this.compositeMaterial;
		{
			const uniforms = this.compositeMaterial.uniforms;
			uniforms["bloomStrength"].value = this.strength;
			uniforms["bloomRadius"].value = this.radius;
			uniforms["bloomTintColors"].value = this.bloomTintColors;
		}

		renderer.setRenderTarget(this.renderTargetsHorizontal[0]);
		renderer.clear();
		this.fsQuad.render(renderer);

		// blend it additively over the input texture

		this.fsQuad.material = this.materialCopy;
		this.copyUniforms[
			"tDiffuse"
		].value = this.renderTargetsHorizontal[0].texture;

		if (maskActive) renderer.context.enable(renderer.context.STENCIL_TEST);

		renderer.setRenderTarget(this.renderToScreen ? null : readBuffer);
		this.fsQuad.render(renderer);

		renderer.setClearColor(this.oldClearColor, this.oldClearAlpha);
		renderer.autoClear = oldAutoClear;
	}

	getSeperableBlurMaterial(kernelRadius) {
		return new ShaderMaterial({
			defines: {
				KERNEL_RADIUS: kernelRadius,
				SIGMA: kernelRadius
			},
			uniforms: {
				colorTexture: { value: null },
				texSize: { value: new Vector2(0.5, 0.5) },
				direction: { value: new Vector2(0.5, 0.5) }
			},
			vertexShader,
			fragmentShader: separableblurFragmentShader
		});
	}

	getCompositeMaterial(nMips) {
		return new ShaderMaterial({
			defines: {
				NUM_MIPS: nMips
			},
			uniforms: {
				blurTexture1: { value: null },
				blurTexture2: { value: null },
				blurTexture3: { value: null },
				blurTexture4: { value: null },
				blurTexture5: { value: null },
				dirtTexture: { value: null },
				bloomStrength: { value: 1.0 },
				bloomFactors: { value: null },
				bloomTintColors: { value: null },
				bloomRadius: { value: 0.0 }
			},
			vertexShader,
			fragmentShader: compositeFragmentShader
		});
	}
}

import Pass from "./Pass";

export default class MaskPass extends Pass {
	constructor(scene, camera) {
		super();

		this.scene = scene;
		this.camera = camera;

		this.clear = true;
		this.needsSwap = false;

		this.inverse = false;
	}

	render(renderer, writeBuffer, readBuffer) {
		const { context, state } = renderer;
		const { color, depth, stencil } = state.buffers;

		// don't update color or depth
		color.setMask(false);
		depth.setMask(false);

		// lock buffers
		color.setLocked(true);
		depth.setLocked(true);

		// setup stencil
		stencil.setTest(true);
		stencil.setOp(context.REPLACE, context.REPLACE, context.REPLACE);
		stencil.setFunc(context.ALWAYS, this.inverse ? 0 : 1, 0xffffffff);
		stencil.setClear(this.inverse ? 1 : 0);

		// draw into the stencil buffer
		renderer.setRenderTarget(readBuffer);
		if (this.clear) renderer.clear();
		renderer.render(this.scene, this.camera);

		renderer.setRenderTarget(writeBuffer);
		if (this.clear) renderer.clear();
		renderer.render(this.scene, this.camera);

		// unlock color and depth buffers
		color.setLocked(false);
		depth.setLocked(false);

		// draw if equal to 1
		stencil.setFunc(context.EQUAL, 1, 0xffffffff);
		stencil.setOp(context.KEEP, context.KEEP, context.KEEP);
	}
}

export class ClearMaskPass extends Pass {
	constructor() {
		super();
		this.needsSwap = false;
	}

	render(renderer) {
		renderer.state.buffers.stencil.setTest(false);
	}
}

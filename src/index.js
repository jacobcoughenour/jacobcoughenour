import "tippy.js/dist/tippy.css";
import "tippy.js/themes/material.css";
import "tippy.js/dist/backdrop.css";
import "tippy.js/animations/shift-away.css";
import "./global.scss";

import tippy, { animateFill, followCursor } from "tippy.js";

import App from "./App.svelte";

tippy.setDefaultProps({
	theme: "material",
	plugins: [animateFill, followCursor],
	placement: "bottom"
});

new App({
	target: document.getElementById("root"),
	props: {}
});
